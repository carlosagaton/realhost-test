<?php

include_once './Classes/XML.php';
include_once './Classes/Comprobante.php';
include_once './Classes/Emisor.php';

class CFDI
{
    public static $comprobante;
    protected static $xml;
    public static $emisor;

    function __construct()
    {
        self::$comprobante = new Comprobante();
        self::$emisor = new Emisor();
    }

    static function setAttribute($model, $attr, $value) 
    {
        if ($model instanceof Comprobante) {
            self::$comprobante->setAtribute($attr, $value);
        } elseif ($model instanceof Emisor) {
            self::$emisor->setAtribute($attr, $value);
        }
    }


    static function getNode()
    {
        self::$xml = '<?xml version="1.0" encoding="UTF-8"?> <cfdi:Comprobante  xmlns:cfdi="http://www.sat.gob.mx/cfd/3"  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd" ' . self::$comprobante->getAtributes() . ' >';
        self::$xml .= self::$emisor->getNode(); 
        self::$xml .= '</cfdi:Comprobante>';
        return self::$xml;
    }
}
<?php

include_once './CFDI.php';

class Main
{
    protected static $cfdi_xml;
    protected static $array_data = [
        "Comprobante" => [
            "LugarExpedicion" => "64000",
            "TipoDeComprobante" => "i",
            "Moneda" => "MXN",
            "SubTotal" => "100",
            "Total" => "116",
            "FormaPago" => "01",
            "NoCertificado" => "00000010101010101",
            "Fecha" => "2021-10-06 11:00:00"
        ],
        "Emisor" => [
            "Rfc" => "TME960709LR2",
            "Nombre" => "Tracto Camiones",
            "RegimenFiscal" => "612"
        ]
    ];

    public function __construct()
    {
        self::$cfdi_xml = new CFDI;
    }

    final public static function createXML()
    {
         //Obtener el XML por medio de la clase XML
        foreach (self::$array_data as $key => $value) :
            if ($key != (string) 'Comprobante') :
                foreach ($value as $attribute => $value) :
                    self::$cfdi_xml::setAttribute(self::$cfdi_xml::$emisor, $attribute, $value);
                endforeach;
            else: 
                foreach ($value as $attribute => $value) :
                    self::$cfdi_xml::setAttribute(self::$cfdi_xml::$comprobante, $attribute, $value);
                endforeach;
            endif;
        endforeach;

        $result = self::$cfdi_xml::getNode();
        $response = htmlspecialchars($result);

        echo '"'. $response. '"';
    }
}

try {
    $main = new Main;
    $main->createXML();
} catch (\Exception $error) {
    echo $error->getMessage();
}
